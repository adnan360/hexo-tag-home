/**
 * Just returns the home url from Hexo config.
 */
hexo.extend.tag.register('home_url', function(args){
  return hexo.config.url;
});

/**
 * Returns an HTML <a> tag for homepage (with an option to add string to URL).
 */
hexo.extend.tag.register('home', function(args){
  var label  = args[0];
  if ( ! ( label ) ) {
    label = hexo.config.url;
  }
  var extend = encodeURI(args[1]).toString();
  var url    = hexo.config.url;
  if ( extend ) {
    url = url + extend;
  }

  return '<a href=' + url + '>' + label + '</a>';
});
