# hexo-tag-home

A tag plugin for Hexo to either show the homepage url from Hexo config or URLify it.

Offers 2 tags:
- `home_url`: Simply returns the homepage URL. No links, no formatting.
- `home`: URLifies the homepage URL.


## Install

```bash
cd /path/to/hexo/project
npm install https://gitlab.com/adnan360/hexo-tag-home
# to update
npm update
```


## Example

```md
{% home_url %}
{% home %}
{% home 'test' %}
{% home 'test' '/faq' %}
```

Output:

```html
https://example.gitlab.io
<a href=https://example.gitlab.io>https://example.gitlab.io</a>
<a href=https://example.gitlab.io>test</a>
<a href=https://example.gitlab.io/faq>test</a>
```


## License

MIT Expat License. See `LICENSE`.
